package com.example.h2database.modelsOne.oneToManyUnidirectional;

import javax.persistence.*;

@Entity
@Table(name="student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String admNumber;

    public Student() {};

    public Student(String admNumber) {
        this.admNumber = admNumber;
    }

    public String getAdmNumber() {
        return admNumber;
    }

    public void setAdmNumber(String admNumber) {
        this.admNumber = admNumber;
    }
}
