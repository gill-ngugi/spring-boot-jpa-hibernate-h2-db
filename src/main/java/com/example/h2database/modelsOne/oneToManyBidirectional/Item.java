package com.example.h2database.modelsOne.oneToManyBidirectional;

import javax.persistence.*;

@Entity
@Table(name="item ")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String serialNumber;
    @ManyToOne
    @JoinColumn(name = "cart_id") //Item table will have a column -> cart_id
    private Cart cart;

    public Item() {

    }

    public Item(String serialNumber, Cart cart) {
        this.serialNumber = serialNumber;
        this.cart = cart;
    }
}
