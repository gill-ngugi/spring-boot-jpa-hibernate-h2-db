package com.example.h2database.modelsOne.manyToManyBidirectional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "viewer")
public class Viewer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;
    private String nickname;
    @ManyToMany
    @JoinTable(
            // Name of the association table - Table that stores foreign key for both entities
            name = "followed_streams",
            // Column that stores the foreign key  of the owning side
            joinColumns = @JoinColumn(name = "viewer_id"),
            // Column that stores the foreign key of the inverse side
            inverseJoinColumns = @JoinColumn(name = "stream_id")
    )
    private List<Stream> followedStreams = new ArrayList<>();

    public Viewer(String nickname) {
        this.nickname = nickname;
    }

    public void followStream(Stream stream) {
        followedStreams.add(stream);
    }
}