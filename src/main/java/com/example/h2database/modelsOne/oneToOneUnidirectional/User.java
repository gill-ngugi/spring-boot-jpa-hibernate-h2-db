package com.example.h2database.modelsOne.oneToOneUnidirectional;

import javax.persistence.*;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToOne
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    public User() {

    }

    public User(String name, Address address) {
        this.name = name;
        this.address = address;
    }
}
