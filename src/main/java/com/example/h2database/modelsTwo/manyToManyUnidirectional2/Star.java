package com.example.h2database.modelsTwo.manyToManyUnidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "star")
public class Star {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "star_pk_id")
    private Long id;
    @Column(name = "star_name")
    private String name;
    @Column(name = "star_color")
    private String color;

    public Star() {

    }

    public Star(String name, String color) {
        this.name = name;
        this.color = color;
    }

}
