package com.example.h2database.modelsTwo.manyToManyUnidirectional2;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "constellation")
public class Constellation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "constellation_name")
    private String name;
    @Column(name = "zodiac_sign")
    private String zodiac;
    @ManyToMany
    @JoinTable(
            name = "constellation_star",
            joinColumns = {
                    @JoinColumn(name = "constellation_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "star_id", referencedColumnName = "star_pk_id")
            }
    )
    private List<Star> stars = new ArrayList<>();

    public Constellation() {

    }

    public Constellation(String name, String zodiac) {
        this.name = name;
        this.zodiac = zodiac;
    }

    public void addStar(Star star) {
        stars.add(star);
    }
}
