package com.example.h2database.modelsTwo.manyToOneUnidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @ManyToOne(
            cascade = CascadeType.ALL
    )
//    @JoinColumn(name = "bible_id", referencedColumnName = "id")
    @JoinTable(
            name = "bible_book",
            joinColumns = {
                    @JoinColumn(name = "book_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "bible_id", referencedColumnName = "id")
            }
    )
    private Bible bible;

    public Book() {

    }

    public Book(String name, Bible bible) {
        this.name = name;
        this.bible = bible;
    }

    public void setBible(Bible bible) {
        this.bible = bible;
    }
}
