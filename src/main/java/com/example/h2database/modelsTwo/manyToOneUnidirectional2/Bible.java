package com.example.h2database.modelsTwo.manyToOneUnidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "bible")
public class Bible {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    public Bible() {

    }

    public Bible(String name) {
        this.name = name;
    }
}
