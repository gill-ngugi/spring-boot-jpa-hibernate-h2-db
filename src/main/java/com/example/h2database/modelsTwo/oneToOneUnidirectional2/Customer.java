package com.example.h2database.modelsTwo.oneToOneUnidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToOne(
            cascade = CascadeType.ALL
    )
//    @JoinColumn(name = "item_id", referencedColumnName = "id")
    @JoinTable(
            name = "customer_item2",
            joinColumns = {
                @JoinColumn(name = "customer_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "item2_id", referencedColumnName = "id")
            }
    )
    private Item2 item2;

    public Customer() {

    }

    public Customer(String name) {
        this.name = name;
    }

    public Item2 getItem2() {
        return item2;
    }

    public void setItem2(Item2 item2) {
        this.item2 = item2;
    }
}
