package com.example.h2database.modelsTwo.oneToOneUnidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "item2")
public class Item2 {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    public Item2() {

    }

    public Item2(String name) {
        this.name = name;
    }
}
