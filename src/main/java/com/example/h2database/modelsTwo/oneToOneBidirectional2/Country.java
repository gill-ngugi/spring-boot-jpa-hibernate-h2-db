package com.example.h2database.modelsTwo.oneToOneBidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "country")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String continent;
    @OneToOne(
            mappedBy = "country",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private President president;

    public Country() {

    }

    public Country(String name, String continent) {
        this.name = name;
        this.continent = continent;
    }

    public President getPresident() {
        return president;
    }

    public void setPresident(President president) {
        this.president = president;
    }
}
