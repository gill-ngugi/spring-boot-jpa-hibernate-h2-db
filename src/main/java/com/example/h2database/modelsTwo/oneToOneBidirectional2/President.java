package com.example.h2database.modelsTwo.oneToOneBidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "president")
public class President {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToOne(
            cascade = CascadeType.ALL
    )
//    @JoinColumn(name = "country_id", referencedColumnName = "id")
    @JoinTable(
            name = "president_country",
            joinColumns = {
                @JoinColumn(name = "president_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                @JoinColumn(name = "country_id", referencedColumnName = "id")
            }
    )
    private Country country;

    public President() {

    }

    public President(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
