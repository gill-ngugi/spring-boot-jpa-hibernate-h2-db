package com.example.h2database.modelsTwo.manyToManyBidirectional2;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String tutor;
    @ManyToMany
    @JoinTable(
            name = "course_pupil",
            joinColumns = {
                    @JoinColumn(name="course_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name="pupil_id")
            }
    )
    private List<Pupil> pupils = new ArrayList<>();

    public Course() {

    }

    public Course(String name, String tutor) {
        this.name = name;
        this.tutor = tutor;
    }

    public void addPupil(Pupil pupil) {
        pupils.add(pupil);
    }
}
