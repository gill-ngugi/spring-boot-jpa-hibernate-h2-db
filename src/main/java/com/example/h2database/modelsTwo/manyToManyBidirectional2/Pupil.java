package com.example.h2database.modelsTwo.manyToManyBidirectional2;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "pupil")
public class Pupil {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String age;
    @ManyToMany(
            mappedBy = "pupils"
    )
    private List<Course> courses = new ArrayList<>();

    public Pupil() {

    }

    public Pupil(String name, String age) {
        this.name = name;
        this.age = age;
    }

    public void addCourse(Course course) {
        courses.add(course);
    }
}

