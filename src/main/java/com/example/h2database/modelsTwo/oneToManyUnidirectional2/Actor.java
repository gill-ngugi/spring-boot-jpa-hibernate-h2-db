package com.example.h2database.modelsTwo.oneToManyUnidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "actor")
public class Actor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    public Actor() {

    }

    public Actor(String name) {
        this.name = name;
    }
}
