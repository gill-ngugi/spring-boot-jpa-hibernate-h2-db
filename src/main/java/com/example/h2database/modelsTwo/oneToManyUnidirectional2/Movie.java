package com.example.h2database.modelsTwo.oneToManyUnidirectional2;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "movie")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String genre;
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
//    @JoinColumn(name="movie_id")
    @JoinTable(
            name = "movie_actor",
            joinColumns = {
                    @JoinColumn(name = "movie_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "actor_id", referencedColumnName = "id")
            }
    )

    private List<Actor> actors = new ArrayList<>();

    public Movie() {

    }

    public Movie(String name, String genre) {
        this.name = name;
        this.genre = genre;
    }

    public Movie(String name, String genre, List<Actor> actors) {
        this.name = name;
        this.genre = genre;
        this.actors = actors;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }
}
