package com.example.h2database.modelsTwo.manyToOneBidirectional2;

import javax.persistence.*;

@Entity
@Table(name = "song")
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String artistName;
    @ManyToOne(
            cascade = CascadeType.ALL
    )
//    @JoinColumn(name = "playlist_id", referencedColumnName = "id")
    @JoinTable(
            name = "playlist_song",
            joinColumns = {
                    @JoinColumn(name = "song_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "playlist_id")
            }
    )
    private Playlist playlist;

    public Song() {

    }

    public Song(String name, String artistName, Playlist playlist) {
        this.name = name;
        this.artistName = artistName;
        this.playlist = playlist;
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    public void setPlaylist(Playlist playlist) {
        this.playlist = playlist;
    }

}
