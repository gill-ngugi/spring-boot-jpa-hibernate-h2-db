package com.example.h2database;

import com.example.h2database.modelsTwo.oneToOneUnidirectional2.Customer;
import com.example.h2database.modelsTwo.oneToOneUnidirectional2.Item2;
import com.example.h2database.repositoryTwo.oneToOneUnidirectionalRepo2.CustomerRepository2;
import com.example.h2database.repositoryTwo.oneToOneUnidirectionalRepo2.ItemRepository2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class OneToOneUnidirectionalRunner2 {

    public static void main(String[] args) {

        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(OneToOneUnidirectionalRunner2.class, args);
        CustomerRepository2 customerRepository2 = configurableApplicationContext.getBean(CustomerRepository2.class);
        ItemRepository2 itemRepository2 = configurableApplicationContext.getBean(ItemRepository2.class);

        Item2 item2 = new Item2("Backpack");
        Customer customer = new Customer("Europa");
        customer.setItem2(item2);
        customerRepository2.save(customer);
    }

}
