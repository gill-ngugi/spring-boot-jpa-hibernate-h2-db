package com.example.h2database;

import com.example.h2database.modelsTwo.oneToManyUnidirectional2.Actor;
import com.example.h2database.modelsTwo.oneToManyUnidirectional2.Movie;
import com.example.h2database.repositoryTwo.oneToManyBidirectionalRepo2.ActorRepo2;
import com.example.h2database.repositoryTwo.oneToManyBidirectionalRepo2.MovieRepo2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class OneToManyUnidirectionalRunner2 {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(OneToManyUnidirectionalRunner2.class, args);
        MovieRepo2 movieRepo2 = configurableApplicationContext.getBean(MovieRepo2.class);
        ActorRepo2 actorRepo2 = configurableApplicationContext.getBean(ActorRepo2.class);

        Movie movie = new Movie("WW84", "Action");
        Actor gal = new Actor("Gal Gadot");
        Actor pine = new Actor("Chris Pine");
        List<Actor> actors = Arrays.asList(gal, pine);
        movie.setActors(actors);
        movieRepo2.save(movie);

        Actor dwayne = new Actor("Dwayne Johnson");
        Actor reynolds = new Actor("Ryan Reynolds");
        List<Actor> actors1 = Arrays.asList(dwayne, reynolds);
        Movie redNotice = new Movie("Red Notice", "Action", actors1);
        movieRepo2.save(redNotice);
    }

}
