package com.example.h2database;

import com.example.h2database.modelsTwo.manyToOneBidirectional2.Playlist;
import com.example.h2database.modelsTwo.manyToOneBidirectional2.Song;
import com.example.h2database.repositoryTwo.manyToOneBidirectionalRepo2.PlaylistRepo2;
import com.example.h2database.repositoryTwo.manyToOneBidirectionalRepo2.SongRepo2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class ManyToOneBidirectionalRunner2 {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(ManyToOneBidirectionalRunner2.class, args);
        PlaylistRepo2 playlistRepo2 = configurableApplicationContext.getBean(PlaylistRepo2.class);
        SongRepo2 songRepo2 = configurableApplicationContext.getBean(SongRepo2.class);

        Playlist pop = new Playlist("Pop", "Chill");
        Song song1 = new Song("7 rings", "Ariana Grande", pop);
        Song song2 = new Song("Shivers", "Ed Sheeran", pop);
        Song song3 = new Song("Bam Bam", "Camila Cabello", pop);
        List<Song> songs = Arrays.asList(song1, song2, song3);
        pop.setSongs(songs);
        playlistRepo2.save(pop);

        Playlist edm = new Playlist("EDM", "Hype");
        Song song4 = new Song("Red Lights","Tiesto", edm);
        Song song5 = new Song("Faded", "Alan Walker", edm);
        List<Song> songs1 = Arrays.asList(song4, song5);
        songRepo2.saveAll(songs1);
    }
}
