package com.example.h2database;

import com.example.h2database.modelsOne.Person;
import com.example.h2database.repositoryOne.PersonRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class PersonRunner {
    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(PersonRunner.class, args);
        PersonRepository personRepository = configurableApplicationContext.getBean(PersonRepository.class);
		Person myPerson = new Person("Ontario", "Canada");
		personRepository.save(myPerson);
    }

//  To use these lines of code, comment the whole main function first and uncomment lines 18-24
//	@Bean
//	CommandLineRunner commandLineRunner (PersonRepository personRepository) {
//		return args -> {
//			Person Jay = new Person("Jay", "Kindest");
//			personRepository.save(Jay);
//		};
//	}
}
