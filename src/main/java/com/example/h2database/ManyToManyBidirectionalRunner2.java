package com.example.h2database;

import com.example.h2database.modelsTwo.manyToManyBidirectional2.Course;
import com.example.h2database.modelsTwo.manyToManyBidirectional2.Pupil;
import com.example.h2database.repositoryTwo.manyToManyBidirectionalRepo2.CourseRepo2;
import com.example.h2database.repositoryTwo.manyToManyBidirectionalRepo2.PupilRepo2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class ManyToManyBidirectionalRunner2 {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(ManyToManyBidirectionalRunner2.class, args);
        CourseRepo2 courseRepo2 = configurableApplicationContext.getBean(CourseRepo2.class);
        PupilRepo2 pupilRepo2 = configurableApplicationContext.getBean(PupilRepo2.class);

        Course Art = new Course("Art", "Herman");
        Course Coding = new Course("Coding", "Gillian");
        Course Baking = new Course("Baking", "Isaac");
        List<Course> courseList = Arrays.asList(Art, Coding, Baking);

        // Scenario One - Courses create pupils
        Pupil Jay = new Pupil("Jay", "28");
        Pupil Neil = new Pupil("Neil", "29");
        Pupil Kasey = new Pupil("Kasey", "27");
        Pupil Tyson = new Pupil("Tyson", "28");
        List<Pupil> pupilList = Arrays.asList(Jay, Neil, Kasey, Tyson);
        pupilRepo2.saveAll(pupilList);

        Art.addPupil(Jay);
        Art.addPupil(Neil);
        Coding.addPupil(Jay);
        Coding.addPupil(Neil);
        Baking.addPupil(Kasey);
        Art.addPupil(Tyson);
        courseRepo2.saveAll(courseList);

        // Scenario Two
        Course Swimming = new Course("Swimming", "Wesley");
        Course Painting = new Course("Painting", "Ben");
        List<Course> courseList1 = Arrays.asList(Swimming, Painting);
        courseRepo2.saveAll(courseList1);

        Pupil Gill = new Pupil("Gill", "26");
        Pupil Lesley = new Pupil("Lesley", "26");
        List<Pupil> pupilList1 = Arrays.asList(Gill, Lesley);

        Gill.addCourse(Swimming);
        Lesley.addCourse(Painting);
        Gill.addCourse(Painting);
        pupilRepo2.saveAll(pupilList1);
    }

}
