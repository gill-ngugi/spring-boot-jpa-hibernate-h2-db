package com.example.h2database;

import com.example.h2database.modelsTwo.manyToManyUnidirectional2.Constellation;
import com.example.h2database.modelsTwo.manyToManyUnidirectional2.Star;
import com.example.h2database.repositoryTwo.manyToManyUnidirectionalRepo2.ConstellationRepo2;
import com.example.h2database.repositoryTwo.manyToManyUnidirectionalRepo2.StarRepo2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class ManyToManyUnidirectionalRunner2 {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(ManyToManyUnidirectionalRunner2.class, args);
        ConstellationRepo2 constellationRepo2 = configurableApplicationContext.getBean(ConstellationRepo2.class);
        StarRepo2 starRepo2 = configurableApplicationContext.getBean(StarRepo2.class);

        Constellation Sag = new Constellation("Sagittarius", "The Archer");
        Constellation Leo = new Constellation("Leo", "The Lion");
        Constellation Virgo = new Constellation("Virgo", "The Maiden");
        List<Constellation> constellations = Arrays.asList(Sag, Leo, Virgo);

        Star Kaus_Australis = new Star("Kaus Australis", "white");
        Star Ascella = new Star("Ascella", "blue_white");
        Star Regulus = new Star("Regulus", "blue_white");
        Star Spica = new Star("Spica", "white");
        List<Star> stars = Arrays.asList(Kaus_Australis, Ascella, Regulus, Spica);
        starRepo2.saveAll(stars);

        Sag.addStar(Kaus_Australis);
        Sag.addStar(Ascella);
        Leo.addStar(Regulus);
        Virgo.addStar(Spica);
        constellationRepo2.saveAll(constellations);
    }

}
