package com.example.h2database;

import com.example.h2database.modelsTwo.oneToOneBidirectional2.Country;
import com.example.h2database.modelsTwo.oneToOneBidirectional2.President;
import com.example.h2database.repositoryTwo.oneToOneBidirectionalRepo2.CountryRepo2;
import com.example.h2database.repositoryTwo.oneToOneBidirectionalRepo2.PresidentRepo2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class oneToOneBidirectionalRunner2 {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(oneToOneBidirectionalRunner2.class, args);
        PresidentRepo2 presidentRepo2 = configurableApplicationContext.getBean(PresidentRepo2.class);
        CountryRepo2 countryRepo2 = configurableApplicationContext.getBean(CountryRepo2.class);

        // In this case, the country and president are persisted in the DB and the country_id is saved in the president table
        Country country = new Country("U.S.A", "North America");
        President president = new President("Barrack Obama");
        president.setCountry(country);
        presidentRepo2.save(president);

        //In this case, the country and president are persisted in the DB, however, the country_id is NOT saved in the President table.
        //This is because we are using the inverse entity to set the owning entity and also using the inverse's repository to persist data.
        //Hence, the relationship wont be persisted in the DB.
        President president1 = new President("Kenyatta");
        Country country1 = new Country("Kenya", "Africa");
        country1.setPresident(president1);
        countryRepo2.save(country1);
    }
}
