package com.example.h2database;

import com.example.h2database.modelsOne.manyToManyBidirectional.Stream;
import com.example.h2database.modelsOne.manyToManyBidirectional.Viewer;
import com.example.h2database.repositoryOne.manyToManyBidirectionalRepo.StreamRepository;
import com.example.h2database.repositoryOne.manyToManyBidirectionalRepo.ViewerRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class ManyToManyBidirectionalRunner {

    public static void main(String[] args) {
        ConfigurableApplicationContext  configurableApplicationContext
                = SpringApplication.run(ManyToManyBidirectionalRunner.class, args);
        StreamRepository streamRepository = configurableApplicationContext.getBean(StreamRepository.class);
        ViewerRepository viewerRepository = configurableApplicationContext.getBean(ViewerRepository.class);

        Viewer John = new Viewer("John Doe");
        Viewer Robin = new Viewer("Robin Kelly");
        Viewer Christine = new Viewer("Christine");
        List<Viewer> viewers = Arrays.asList(John, Robin, Christine);

        Stream cool = new Stream("Cool");
        Stream hyper = new Stream("Hyper");
        List<Stream> streams = Arrays.asList(cool, hyper);
        streamRepository.saveAll(streams);


        John.followStream(hyper);
        Robin.followStream(hyper);
        John.followStream(cool);
        Christine.followStream(cool);
        viewerRepository.saveAll(viewers);
    }

}
