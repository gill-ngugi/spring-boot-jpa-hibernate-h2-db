package com.example.h2database;

import com.example.h2database.modelsOne.oneToOneBidirectional.Car;
import com.example.h2database.modelsOne.oneToOneBidirectional.Owner;
import com.example.h2database.repositoryOne.oneToOneBidirectionalRepo.CarRepository;
import com.example.h2database.repositoryOne.oneToOneBidirectionalRepo.OwnerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Optional;

@SpringBootApplication
public class OneToOneBidirectionalRunner {
    private static final Logger log = LoggerFactory.getLogger(OneToOneBidirectionalRunner.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(OneToOneBidirectionalRunner.class, args);
        CarRepository carRepository = configurableApplicationContext.getBean(CarRepository.class);
        OwnerRepository ownerRepository = configurableApplicationContext.getBean(OwnerRepository.class);
        Car car = new Car("m600");
        carRepository.save(car);
        Owner owner = new Owner("Luther");
        owner.setCar(car);
        ownerRepository.save(owner);

        Optional<Car> optionalCar = carRepository.findById(10L);
        Optional<Owner> optionalOwner = ownerRepository.findById(11L);

        if(optionalCar.isPresent() && optionalOwner.isPresent()) {
            log.info(optionalCar.get() + " is owned by " + optionalCar.get().getOwner());
            log.info(optionalOwner.get() + " owns the car " + optionalOwner.get().getCar());
        }
    }

}
