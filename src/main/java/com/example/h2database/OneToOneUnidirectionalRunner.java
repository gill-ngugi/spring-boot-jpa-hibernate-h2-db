package com.example.h2database;

import com.example.h2database.modelsOne.oneToOneUnidirectional.Address;
import com.example.h2database.modelsOne.oneToOneUnidirectional.User;
import com.example.h2database.repositoryOne.oneToOneUnidirectionalRepo.AddressRepository;
import com.example.h2database.repositoryOne.oneToOneUnidirectionalRepo.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class OneToOneUnidirectionalRunner {
    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(OneToOneUnidirectionalRunner.class, args);
        AddressRepository addressRepository = configurableApplicationContext.getBean(AddressRepository.class);
        UserRepository userRepository = configurableApplicationContext.getBean(UserRepository.class);

        Address address = new Address("Mayfair Street");
        addressRepository.save(address);

        User user = new User("Johnnie English", address);
        userRepository.save(user);
    }
}
