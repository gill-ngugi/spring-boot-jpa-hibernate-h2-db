package com.example.h2database.repositoryTwo.manyToOneUnidirectionalRepo2;

import com.example.h2database.modelsTwo.manyToOneUnidirectional2.Bible;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BibleRepo2 extends CrudRepository<Bible, Long> {
}
