package com.example.h2database.repositoryTwo.manyToOneUnidirectionalRepo2;

import com.example.h2database.modelsTwo.manyToOneUnidirectional2.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepo2 extends CrudRepository<Book, Long> {
}
