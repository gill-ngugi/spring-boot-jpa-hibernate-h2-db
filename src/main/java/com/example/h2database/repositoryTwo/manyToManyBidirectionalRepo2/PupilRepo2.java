package com.example.h2database.repositoryTwo.manyToManyBidirectionalRepo2;

import com.example.h2database.modelsTwo.manyToManyBidirectional2.Pupil;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PupilRepo2 extends CrudRepository<Pupil, Long> {
}
