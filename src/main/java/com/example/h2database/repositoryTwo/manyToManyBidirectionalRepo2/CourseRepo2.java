package com.example.h2database.repositoryTwo.manyToManyBidirectionalRepo2;

import com.example.h2database.modelsTwo.manyToManyBidirectional2.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRepo2 extends CrudRepository<Course, Long> {
}
