package com.example.h2database.repositoryTwo.manyToOneBidirectionalRepo2;

import com.example.h2database.modelsTwo.manyToOneBidirectional2.Song;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SongRepo2 extends CrudRepository<Song, Long> {
}
