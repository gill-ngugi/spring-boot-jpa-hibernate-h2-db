package com.example.h2database.repositoryTwo.manyToOneBidirectionalRepo2;

import com.example.h2database.modelsTwo.manyToOneBidirectional2.Playlist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaylistRepo2 extends CrudRepository<Playlist, Long> {
}
