package com.example.h2database.repositoryTwo.oneToOneUnidirectionalRepo2;

import com.example.h2database.modelsTwo.oneToOneUnidirectional2.Item2;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository2 extends CrudRepository<Item2, Long> {
}
