package com.example.h2database.repositoryTwo.oneToOneUnidirectionalRepo2;

import com.example.h2database.modelsTwo.oneToOneUnidirectional2.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository2 extends CrudRepository<Customer, Long> {
}
