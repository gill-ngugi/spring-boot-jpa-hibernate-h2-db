package com.example.h2database.repositoryTwo.oneToManyBidirectionalRepo2;

import com.example.h2database.modelsTwo.oneToManyUnidirectional2.Actor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActorRepo2 extends CrudRepository<Actor, Long> {
}
