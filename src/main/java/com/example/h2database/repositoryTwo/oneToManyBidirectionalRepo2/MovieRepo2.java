package com.example.h2database.repositoryTwo.oneToManyBidirectionalRepo2;

import com.example.h2database.modelsTwo.oneToManyUnidirectional2.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepo2 extends CrudRepository<Movie, Long> {
}
