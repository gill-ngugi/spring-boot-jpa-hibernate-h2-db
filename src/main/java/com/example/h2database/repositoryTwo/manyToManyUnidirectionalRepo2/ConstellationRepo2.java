package com.example.h2database.repositoryTwo.manyToManyUnidirectionalRepo2;

import com.example.h2database.modelsTwo.manyToManyUnidirectional2.Constellation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConstellationRepo2 extends CrudRepository<Constellation, Long> {
}
