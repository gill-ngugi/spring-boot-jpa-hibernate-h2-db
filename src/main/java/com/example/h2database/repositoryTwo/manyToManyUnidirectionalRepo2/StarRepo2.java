package com.example.h2database.repositoryTwo.manyToManyUnidirectionalRepo2;

import com.example.h2database.modelsTwo.manyToManyUnidirectional2.Star;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StarRepo2 extends CrudRepository<Star, Long> {
}
