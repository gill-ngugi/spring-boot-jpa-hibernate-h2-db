package com.example.h2database.repositoryTwo.oneToOneBidirectionalRepo2;

import com.example.h2database.modelsTwo.oneToOneBidirectional2.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepo2 extends CrudRepository<Country, Long> {
}
