package com.example.h2database.repositoryOne.oneToOneBidirectionalRepo;

import com.example.h2database.modelsOne.oneToOneBidirectional.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
}
