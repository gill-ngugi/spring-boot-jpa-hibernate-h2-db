package com.example.h2database.repositoryOne.oneToOneBidirectionalRepo;

import com.example.h2database.modelsOne.oneToOneBidirectional.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OwnerRepository extends CrudRepository<Owner, Long> {
}
