package com.example.h2database.repositoryOne.manyToManyBidirectionalRepo;

import com.example.h2database.modelsOne.manyToManyBidirectional.Viewer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ViewerRepository extends CrudRepository<Viewer, Long> {
}
