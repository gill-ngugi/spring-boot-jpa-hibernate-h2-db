package com.example.h2database.repositoryOne.oneToOneUnidirectionalRepo;

import com.example.h2database.modelsOne.oneToOneUnidirectional.Address;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {
}
