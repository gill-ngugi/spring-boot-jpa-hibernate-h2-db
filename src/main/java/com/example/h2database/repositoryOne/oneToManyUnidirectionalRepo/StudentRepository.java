package com.example.h2database.repositoryOne.oneToManyUnidirectionalRepo;

import com.example.h2database.modelsOne.oneToManyUnidirectional.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
}
