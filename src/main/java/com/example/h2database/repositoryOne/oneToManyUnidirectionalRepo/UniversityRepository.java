package com.example.h2database.repositoryOne.oneToManyUnidirectionalRepo;

import com.example.h2database.modelsOne.oneToManyUnidirectional.University;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UniversityRepository extends CrudRepository<University, Long> {
}
