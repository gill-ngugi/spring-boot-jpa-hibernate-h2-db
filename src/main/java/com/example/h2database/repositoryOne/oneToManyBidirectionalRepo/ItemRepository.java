package com.example.h2database.repositoryOne.oneToManyBidirectionalRepo;

import com.example.h2database.modelsOne.oneToManyBidirectional.Item;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
}
