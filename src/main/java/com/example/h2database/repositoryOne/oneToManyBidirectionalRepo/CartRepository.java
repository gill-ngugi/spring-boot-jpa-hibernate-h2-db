package com.example.h2database.repositoryOne.oneToManyBidirectionalRepo;

import com.example.h2database.modelsOne.oneToManyBidirectional.Cart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository<Cart, Long> {
}
