package com.example.h2database;

import com.example.h2database.modelsTwo.manyToOneUnidirectional2.Bible;
import com.example.h2database.modelsTwo.manyToOneUnidirectional2.Book;
import com.example.h2database.repositoryTwo.manyToOneUnidirectionalRepo2.BibleRepo2;
import com.example.h2database.repositoryTwo.manyToOneUnidirectionalRepo2.BookRepo2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class ManyToOneUnidirectionalRunner2 {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(ManyToOneUnidirectionalRunner2.class, args);
        BookRepo2 bookRepo2 = configurableApplicationContext.getBean(BookRepo2.class);
        BibleRepo2 bibleRepo2 = configurableApplicationContext.getBean(BibleRepo2.class);

        Bible bible = new Bible("Good News Bible");
        Book book1 = new Book("Leviticus", bible);
        Book book2 = new Book("Numbers", bible);
        List<Book> books = Arrays.asList(book1, book2);
        bookRepo2.saveAll(books);

        Bible bible1 = new Bible("King James Bible");
        Book book3 = new Book("Mark", bible1);
        Book book4 = new Book("Luke", bible1);
        List<Book> books1 = Arrays.asList(book3, book4);
        bookRepo2.saveAll(books1);
    }

}
