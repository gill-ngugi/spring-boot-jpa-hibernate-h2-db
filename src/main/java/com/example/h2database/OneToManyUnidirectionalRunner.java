package com.example.h2database;

import com.example.h2database.modelsOne.oneToManyUnidirectional.Student;
import com.example.h2database.modelsOne.oneToManyUnidirectional.University;
import com.example.h2database.repositoryOne.oneToManyUnidirectionalRepo.StudentRepository;
import com.example.h2database.repositoryOne.oneToManyUnidirectionalRepo.UniversityRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class OneToManyUnidirectionalRunner {
    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(OneToManyUnidirectionalRunner.class, args);
        StudentRepository studentRepository = configurableApplicationContext.getBean(StudentRepository.class);
        UniversityRepository universityRepository = configurableApplicationContext.getBean(UniversityRepository.class);

        Student student1 = new Student("111");
        Student student2 = new Student("222");
        List<Student> students = Arrays.asList(student1, student2);
        University university = new University("People Uni", students);
        universityRepository.save(university);

        Student student3 = new Student("333");
        studentRepository.save(student3);

//        universityRepository.delete(university);
    }

}
