package com.example.h2database;

import com.example.h2database.modelsOne.oneToManyBidirectional.Cart;
import com.example.h2database.modelsOne.oneToManyBidirectional.Item;
import com.example.h2database.repositoryOne.oneToManyBidirectionalRepo.CartRepository;
import com.example.h2database.repositoryOne.oneToManyBidirectionalRepo.ItemRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class OneToManyBidirectionalRunner {
    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext =
                SpringApplication.run(OneToManyBidirectionalRunner.class, args);
        CartRepository cartRepository = configurableApplicationContext.getBean(CartRepository.class);
        ItemRepository itemRepository = configurableApplicationContext.getBean(ItemRepository.class);

        Cart cart = new Cart("CartCAD");
        Item chocolate = new Item("CAD-chocolate", cart);
        Item iceCream = new Item("CAD-ice-cream", cart);
        List<Item> items = Arrays.asList(chocolate, iceCream);
        cart.setItems(items);
        cartRepository.save(cart);
        cartRepository.delete(cart);
    }

}
